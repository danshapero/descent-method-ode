\documentclass{article}

\usepackage{amsmath}
%\usepackage{amsfonts}
\usepackage{amsthm}
%\usepackage{amssymb}
%\usepackage{mathrsfs}
%\usepackage{fullpage}
%\usepackage{mathptmx}
%\usepackage[varg]{txfonts}
\usepackage{color}
\usepackage[charter]{mathdesign}
\usepackage[pdftex]{graphicx}
%\usepackage{float}
%\usepackage{hyperref}
%\usepackage[modulo, displaymath, mathlines]{lineno}
%\usepackage{setspace}
%\usepackage[titletoc,toc,title]{appendix}
\usepackage{natbib}

%\linenumbers
%\doublespacing

\theoremstyle{definition}
\newtheorem*{defn}{Definition}
\newtheorem*{exm}{Example}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}
\newtheorem*{lem}{Lemma}
\newtheorem*{prop}{Proposition}
\newtheorem*{cor}{Corollary}

\newcommand{\argmin}{\text{argmin}}
\newcommand{\ud}{\hspace{2pt}\mathrm{d}}
\newcommand{\bs}{\boldsymbol}
\newcommand{\PP}{\mathsf{P}}
\let\divsymb=\div % rename builtin command \div to \divsymb
\renewcommand{\div}[1]{\operatorname{div} #1} % for divergence
\newcommand{\Id}[1]{\operatorname{Id} #1}

\title{Optimization algorithms as ODE timesteppers for gradient flows}
\author{}
\date{}

\begin{document}

\maketitle

Suppose we'd like to solve an optimization problem
\begin{equation}
    u = \text{argmin}\,J
\end{equation}
where $J : V \to \mathbb{R}$ is a smooth convex functional defined on some reflexive Banach space $V$.
We'd like to allow the possibility that $V$ is a Sobolev space and so we'll have to be careful to \emph{not} identity $V$ with its dual space $V^*$.
If $M$ is any self-adjoint, positive-definite linear operator from $V$ to its dual $V^*$ and $\tau$ is some time scale, then we can form the ODE
\begin{equation}
    \tau M\dot u = -\mathrm{d}J(u).
    \label{eq:descent-system}
\end{equation}
The objective functional decreases monotonically along trajectories of this ODE, and the steady state of the system is the solution of the original optimization problem.
This statement remains true if $M$ also depends on $u$.
Different optimization methods can be thought of as different choices for $M$.
Gradient descent amounts to using a single constant operator $M$.
For a problem posed in $L^2(\Omega)$ for some domain $\Omega$, often $M$ is the canonical injection of the space into its dual.
Newton's method, on the other hand, amounts to using $M = \mathrm{d}^2J(u)$.
One can then think of practical descent methods as a particular choice of timestepping scheme for this ODE.
An undamped descent method would correspond to using a constant timestep $\delta t$ and updating $u$ as
\begin{equation}
    u_{n + 1} = u_n - \frac{\delta t}{\tau}M^{-1}\mathrm{d}J(u_n).
\end{equation}
A line search method might then vary the timestep $\delta t_n$ at each step according to some rule.
We can derive yet another variant by applying a linearly implicit Euler discretization to the pure gradient flow from equation \eqref{eq:descent-system}:
\begin{align}
    \tau M\frac{u_{n + 1} - u_n}{\delta t} & = -\mathrm dJ(u_{n + 1}) \nonumber \\
    & \approx -\mathrm dJ(u_n) - \mathrm d^2J(u_n)(u_{n + 1} - u_n)
\end{align}
Rearranging terms a bit, we arrive at the system
\begin{equation}
    u_{n + 1} = u_n - \left(\frac{\tau}{\delta t}M + \mathrm d^2J(u_n)\right)^{-1}\mathrm dJ(u_n)
\end{equation}
which looks more like the implicit regularization afforded by trust region methods.
Small timesteps give us something closer to gradient descent, while large timesteps give us something closer to Newton's method.

What if we instead used a higher-order scheme -- for example RK4?
Could we also use an embedded, adaptive method like RK4/5?
\textbf{Can adaptive timestepping schemes for ODE help us design better line search methods?}

One of the bigger roadblocks to applying Newton's method for practical problems happens when the second derivative of the objective has an eigenvalue very close to zero.
For these types of objectives, trust region methods \citep{conn2000trust} help regularize away some of the difficulty by requiring that the norm of the step is bounded by some factor $\Delta$, and adjusting $\Delta$ based on the decrease in the objective.
\textbf{Is the trust region method also a dynamical system in disguise?}
If so, is this somehow useful or suggestive of better algorithms?
Since we're working in a Banach space, we have to be more explicit about what we mean by the norm of the step.
In the following we'll assume that this norm is induced by some self-adjoint, positive-definite operator $K$.
The appropriate generalization of the trust region condition is that
\begin{equation}
    \tau\|\dot u\|_K \le \Delta
\end{equation}
at all times.
We can then add a Lagrange multiplier $\lambda$ to the problem that enforces this constraint, giving us the differential-algebraic equation
\begin{align}
    \tau(M + \lambda K)\dot u & = -\mathrm{d}J(u), \\
    \lambda(\Delta - \tau\|\dot u\|_K) & = 0, \\
    \lambda & \ge 0, \\
    \Delta - \tau\|\dot u\|_K & \ge 0.
\end{align}
Granted it's a little weird to have an ODE with inequality constraints, but you can think of this like a mechanical system with ``impacts'', see \citet{stewart2011dynamics}.
If we try a naive explicit Euler discretization of this ODE then we get a perfectly acceptable inequality-constrained optimization problem for the state of the system at the next timestep.

This form gives us a way to keep the rate of change of $u$ less than some fixed value of $\Delta$, but how do we adjust $\Delta$?
One way we could think of this is as a \emph{discrete-time control} problem.
At discrete timesteps $\tau$, we adjust the value of $\Delta$ based on the ratio of the actual to predicted decrease in $J$:
\begin{align}
    \text{ared} & = J(u_{n\tau}) - J(u_{(n + 1)\tau}), \nonumber \\
    \text{pred} & = -\left\langle\mathrm{d}J(u_{n\tau}), u_{(n + 1)\tau} - u_{n\tau}\right\rangle \nonumber \\
    & \qquad\quad + \frac{1}{2}\left\langle \mathrm{d}^2J(u_{n\tau})(u_{(n + 1)\tau} - u_{n\tau}), u_{(n + 1)\tau} - u_{n\tau}\right\rangle, \nonumber\\
    \rho_{(n + 1)\tau} & = \text{ared} / \text{pred}.
\end{align}
We can then think of $\Delta_{n\tau}$ as a control that keeps $\rho_{n\tau}$ above some threshold value.
\textbf{Again, can we make trust region methods better or more robust by using knowledge from adaptive ODE solvers?}

Finally, is there some dynamical systems theory that helps you assess how close a trajectory is to steady state?
We can probably assess this numerically by solving the adjoint equation backwards in time.
Can we then get estimates of how close we are to converging after the fact?
A posteriori error analysis is the crown jewel of modern numerical PDE, maybe there's a way to use it.

\pagebreak

\bibliographystyle{plainnat}
\bibliography{descent.bib}

\end{document}
